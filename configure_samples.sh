# Binutils
binutils-2.32/configure --target=$TARGET --prefix=$PREFIX \
--enable-multilib --disable-nls --disable-werror

# GCC First Pass
gcc-9.2.0/configure --target=$TARGET --prefix=$PREFIX --without-headers \
--with-newlib --with-gnu-as --with-gnu-ld \
-enable-interwork --enable-languages="c" --disable-shared \
--disable-libmudflap --disable-libssp --disable-nls \
--disable-decimal-float --enable-altivec --with-cpu=cell

# Newlib
newlib-3.1.0/configure --target=$TARGET --prefix=$PREFIX \
--enable-multilib --disable-nls --enable-newlib-io-long-long \
--enable-newlib-io-long-double

# GCC Second Pass
gcc-9.2.0/configure --target=$TARGET --prefix=$PREFIX \
--with-newlib --with-gnu-as --with-gnu-ld --disable-shared \
--disable-libssp --with-cpu=cell --disable-decimal-float \
--disable-libquadmath --enable-languages=c,c++ --disable-libmudflap \
--disable-nls --disable-linux-futex --enable-altivec \
--disable-threads --disable-libgomp --disable-libitm \
--disable-libatomic --disable-libsanitizer




#######
#Linux#
#######

# Binutils
binutils-2.32/configure --target=$TARGET --prefix=$PREFIX \
--disable-multilib --disable-nls --disable-werror

make
make install

# GCC First Pass
gcc-9.2.0/configure --target=$TARGET --prefix=$PREFIX \
--disable-multilib --with-gnu-as --with-gnu-ld \
-enable-interwork --enable-languages="c" --disable-shared \
--disable-libmudflap --disable-libssp --disable-nls \
--disable-decimal-float --enable-altivec --with-cpu=cell

make all-gcc
make install-gcc

# Glibc First Pass
glibc-2.30/configure --target=$TARGET --prefix=$PREFIX \
--build=$MACHTYPE --host=xenon-linux \
--with-headers=/usr/local/xenon/include/ --disable-multilib \
libc_cv_forced_unwind=yes \
CC=xenon-gcc CXX=xenon-g++

make csu/subdir_lib
install csu/crt1.o csu/crti.o csu/crtn.o $PREFIX/lib

xenon-gcc -nostdlib -nostartfiles -shared -x c /dev/null -o $PREFIX/lib/libc.so
touch $PREFIX/include/gnu/stubs.h

# GCC libgcc
make all-target-libgcc
make install-target-libgcc

# Glibc Second Pass
glibc-2.30/configure --target=$TARGET --prefix=$PREFIX \
--build=$MACHTYPE --host=xenon-linux \
--with-headers=/usr/local/xenon/include/ --disable-multilib \
libc_cv_mabi_ibmlongdouble=yes \
libc_cv_ppc_machine=yes \
libc_cv_mlong_double_128=yes \
libc_cv_forced_unwind=yes \
libc_cv_c_cleanup=yes \
CC=xenon-gcc CXX=xenon-g++

make
make install

# GCC Second Pass
gcc-9.2.0/configure --target=$TARGET --prefix=$PREFIX \
--with-gnu-as --with-gnu-ld --disable-shared \
--disable-libssp --with-cpu=cell --disable-decimal-float \
--disable-libquadmath --enable-languages=c,c++ --disable-libmudflap \
--disable-nls --disable-linux-futex --enable-altivec \
--disable-threads --disable-libgomp --disable-libitm \
--disable-libatomic --disable-libsanitizer --disable-libstdc++-v3

make
make install

# GDB
../gdb-8.3/configure --target=$TARGET \
--host=$MACHTYPE --build=$MACHTYPE \
--prefix=$PREFIX --enable-sim-powerpc \
--enable-sim-stdio --with-headers=$PREFIX/include