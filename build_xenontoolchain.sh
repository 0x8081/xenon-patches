TARGET=xenon
PREFIX=/opt/xenon/
PATH=$PATH:$PREFIX/bin

BINUTIL_TAR="https://ftp.gnu.org/gnu/binutils/binutils-2.32.tar.xz"
GCC_TAR="https://ftp.gnu.org/gnu/gcc/gcc-9.2.0/gcc-9.2.0.tar.xz"
NEWLIB_TAR="ftp://sourceware.org/pub/newlib/newlib-3.1.0.tar.gz"

git clone "https://gitlab.com/0x8081/xenon-patches.git"
cd xenon-patches

wget -O binutils.xz $BINUTIL_TAR
wget -O gcc.xz $GCC_TAR
wget -O newlib.gz $NEWLIB_TAR

tar xf binutils.xz && cat "xenon_binutils-2.32.diff" | patch -p0
tar xf gcc.xz && cat "xenon_gcc-9.2.0.diff" | patch -p0
tar xf newlib.gz && cat "xenon_newlib-3.1.0.diff" | patch -p0

mkdir build
cd build

echo "Building Xenon Binutils..."
../binutils-2.32/configure --target=$TARGET --prefix=$PREFIX \
--enable-multilib --disable-nls --disable-werror

make -j0
sudo make install
rm -rf *

echo "Building Xenon GCC First Pass..."
../gcc-9.2.0/configure --target=$TARGET --prefix=$PREFIX --without-headers \
--with-newlib --with-gnu-as --with-gnu-ld \
-enable-interwork --enable-languages="c" --disable-shared \
--disable-libmudflap --disable-libssp --disable-nls \
--disable-threads --disable-decimal-float --enable-altivec --with-cpu=cell

make -j0 all-gcc
sudo make install-gcc
rm -rf *

echo "Building Xenon Newlib..."
../newlib-3.1.0/configure --target=$TARGET --prefix=$PREFIX \
--enable-multilib --disable-nls --enable-newlib-io-long-long \
--enable-newlib-io-long-double

make -j0
sudo make install
rm -rf *

echo "Building Xenon GCC Final Pass..."
../gcc-9.2.0/configure --target=$TARGET --prefix=$PREFIX \
--with-newlib --with-gnu-as --with-gnu-ld --disable-shared \
--disable-libssp --with-cpu=cell --disable-decimal-float \
--disable-libquadmath --enable-languages=c,c++ --disable-libmudflap \
--disable-nls --disable-linux-futex --enable-altivec \
--disable-threads --disable-libgomp --disable-libitm \
--disable-libatomic --disable-libsanitizer --disable-stdcxx-time \
--disable-lto

make -j0
sudo make install
rm -rf *

echo "Finished!"
